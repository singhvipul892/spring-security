package com.example.demo.auth;

import static com.example.demo.security.ApplicationUserRole.*;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
@Repository
public class ApplicationUserDaoService implements ApplicationUserDao{
	
	private final PasswordEncoder passwordEncoder;	
	
	@Autowired
	public ApplicationUserDaoService(PasswordEncoder passwordEncoder) {
		super();
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public Optional<ApplicationUser> selectUserByUsername(String username) {
		return getApplicationUsers().stream()
				.filter(applicationUser->username.equals(applicationUser.getUsername())).findFirst();
	}
	
    public List<ApplicationUser> getApplicationUsers(){		
    	List<ApplicationUser> applicationUsers=Lists.newArrayList(
    			new ApplicationUser(STUDENT.getGrantedAuthorties(),"Mickey",passwordEncoder.encode("vipul"), true,true, true, true),
    			new ApplicationUser(ADMIN.getGrantedAuthorties(),"Ketty",passwordEncoder.encode("archna"), true,true, true, true),
    			new ApplicationUser(ADMINTRAINEE.getGrantedAuthorties(),"Hobbit",passwordEncoder.encode("robin"), true,true, true, true));
    	return applicationUsers;
	}

}
