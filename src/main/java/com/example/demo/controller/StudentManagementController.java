package com.example.demo.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Student;

@RestController
@RequestMapping("/management/api/v1/students")
public class StudentManagementController {

	private static final List<Student> STUDENTS=(List<Student>) Arrays.asList(
			new Student(1, "Vipul"),
			new Student(2,"Archna"),
			new Student(3, "Robin"));
	
	@GetMapping
	@PreAuthorize("hasAnyRole('ROLE_STUDENT','ROLE_ADMIN','ROLE_ADMINTRAINEE')")
	public List<Student> getStudent() {
		System.out.println("getStudent");
		return STUDENTS; 
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('student:write')")
	public void registerStudent(Student student) {
	  System.out.println("registerStudent");	
	  System.out.println(student); 
	}
	
	@PutMapping(path="{studentId}")
	@PreAuthorize("hasAuthority('student:write')")
	public void updateStudent(@PathVariable Integer studentId,@RequestBody Student student) {
	  System.out.println("updateStudent");	
	  System.out.println(student); 
	}
	
	@DeleteMapping(path="{studentId}")
	@PreAuthorize("hasAuthority('student:write')")
	public void deleteStudent(@PathVariable Integer studentId) {
		  System.out.println("deleteStudent");
		  System.out.println(studentId); 
	}

}
