package com.example.demo.controller;

import java.util.List;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Student;

@RestController
@RequestMapping("api/v1/students")
public class StudentController {

	private static final List<Student> STUDENTS=(List<Student>) Arrays.asList(
			new Student(1, "Vipul"),
			new Student(2,"Archna"),
			new Student(3, "Robin"));
	
	@GetMapping(path = "{studentId}")
	public Student getStudent(@PathVariable Integer studentId) {
		System.out.println(STUDENTS);
		return STUDENTS.stream()
				.filter(student-> studentId.equals(student.getStudentId()))
				.findFirst()
				.orElseThrow(); 
	}
}
