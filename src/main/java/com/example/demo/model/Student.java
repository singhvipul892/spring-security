package com.example.demo.model;

public class Student {

	private Integer studentId;
	private String studentName;
	public Student(Integer studentId, String studentName) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "student id: "+this.studentId+" student name : "+this.studentName;
	}
	
	
}
